package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"os"
)

var config Config

func init() {
	homeDir, _ := os.UserHomeDir()
	path := homeDir + string(os.PathSeparator) + ".dm" + string(os.PathSeparator) + "config.yaml"
	config.InitConfig(path)
}

func main() {
	var musicURL string
	app := cli.NewApp()
	app.Usage = "Music Downloader 支持 网易云音乐、酷我音乐、酷狗音乐"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "url",
			Usage:       "要下载的音乐地址",
			Aliases:     []string{"u", "s"},
			Destination: &musicURL,
		},
	}
	// 命令行分发
	app.Action = func(context *cli.Context) error {
		if len(musicURL) > 0 {
			// 传入下载器
			Downloader(musicURL)
		} else {
			fmt.Println("缺少音乐地址")
		}
		return nil
	}

	_ = app.Run(os.Args)
}
