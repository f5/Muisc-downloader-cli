package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
)

// 酷我音乐结构体
type KMusic struct {
	Data Data `json:"data"`
}

// 音乐信息结构体
type Data struct {
	Musicrid  string `json:"musicrid"`
	Artist    string `json:"artist"`
	Pic       string `json:"pic"`
	Album     string `json:"album"`
	Albumpic  string `json:"albumpic"`
	Pic120    string `json:"pic120"`
	Albuminfo string `json:"albuminfo"`
	Name      string `json:"name"`
}

// 音乐名称
func (k *KMusic) getName() string {
	return k.Data.Name
}

// 获取歌手
func (k *KMusic) getSinger() string {
	return k.Data.Artist
}

// 获取专辑名称
func (k *KMusic) getAlbumName() string {
	return k.Data.Album
}

// 获取封面
func (k *KMusic) getPic() string {
	return k.Data.Pic
}

// 获取音乐下载地址
func (k *KMusic) getDownloadURL() string {
	playURL := fmt.Sprintf(
		"https://antiserver.kuwo.cn/anti.s?type=convert_url&rid=%s&format=mp3&response=url", k.Data.Musicrid)
	response := Get(playURL)

	defer response.Body.Close()

	downURL, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("歌曲下载地址获取失败")
	}
	return string(downURL)
}

// 歌曲详情获取 http://wapi.kuwo.cn/api/www/music/musicInfo?mid=625155&httpsStatus=1
// 歌曲分享地址 https://m.kuwo.cn/yinyue/625155?f=mac&t=usercopy
//			   https://www.kuwo.cn/yinyue/163982269?f=mac&t=usercopy
// 歌曲播放地址获取
func parseByKw(musicURL string) (music *Music) {

	regx := regexp.MustCompile(`https://(m|www)\.kuwo\.cn/yinyue/(\d+)`)

	songId := regx.FindStringSubmatch(musicURL)
	if len(songId) < 2 {
		panic("音乐地址解析失败")
		return
	}

	kmusic, _ := getMusicInfoByKw(songId[2])

	return &Music{
		Name:     kmusic.getName(),
		URL:      kmusic.getDownloadURL(),
		AlbumPic: kmusic.getPic(),
		Album:    kmusic.getAlbumName(),
		Singer:   kmusic.getSinger(),
	}
}

// 获取音乐结构
func getMusicInfoByKw(songId string) (kmusic *KMusic, err error) {
	detailURL := fmt.Sprintf("http://wapi.kuwo.cn/api/www/music/musicInfo?mid=%s&httpsStatus=1", songId)

	response := Get(detailURL)

	defer response.Body.Close()

	result, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("读取页面信息失败")
		return
	}

	err = json.Unmarshal(result, &kmusic)

	if err != nil {
		fmt.Println(err)
		return
	}

	return kmusic, nil

}
