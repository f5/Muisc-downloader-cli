module dm

go 1.16

require (
	github.com/bogem/id3v2 v1.2.0
	github.com/schollz/progressbar/v3 v3.8.2
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.4.0
)
