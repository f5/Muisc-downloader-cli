package main

import (
	"fmt"
	"net/url"
	"os"
	"regexp"
	"strings"
)

type Platform int

const (
	PlatformNetEasy Platform = 1
	PlatformKuWo    Platform = 2
	PlatformKuGou   Platform = 3
)

// 解析URL
func ParseMusic(musicURL string) (Platform Platform) {
	tURL, err := url.Parse(musicURL)
	if err != nil {
		fmt.Println("无效的地址")
		return
	}
	if isNetEasy(tURL.Host) {
		return PlatformNetEasy
	} else if isKuWo(tURL.Host) {
		return PlatformKuWo
	} else if isKuGou(tURL.Host) {
		return PlatformKuGou
	} else {
		fmt.Println("暂未支持该平台")
		os.Exit(1)
	}
	return

}

// 网易云域名
// https://music.163.com/song?id=27588744&userid=59702310
func isNetEasy(domain string) bool {
	return strings.EqualFold("music.163.com", domain)
}

// 酷我域名
// https://m.kuwo.cn/yinyue/625155?f=mac&t=usercopy
// https://www.kuwo.cn/yinyue/163982269?f=mac&t=usercopy
func isKuWo(domain string) bool {
	return strings.EqualFold("m.kuwo.cn", domain) || strings.EqualFold("www.kuwo.cn", domain)
}

// https://www.kugou.com/song/#hash=93B00123D709E9FD61AEF01CB1647C98&album_id=47549237
// https://t1.kugou.com/song.html?id=sjVh4fy0V3
func isKuGou(domain string) bool {
	reg := regexp.MustCompile(`(.*?)\.(kugou)\.com`)

	platform := reg.FindStringSubmatch(domain)
	return platform[2] == "kugou"
}
