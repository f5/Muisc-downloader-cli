BINARY_NAME := dm
VERSION := v1.0.0

default:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_linux_release_$(VERSION) && upx -9 bin/$(BINARY_NAME)_linux_release_$(VERSION) && \
    CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_windows_release_$(VERSION).exe && upx -9 bin/$(BINARY_NAME)_windows_release_$(VERSION).exe && \
    CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_darwin_release_$(VERSION) && upx -9 bin/$(BINARY_NAME)_darwin_release_$(VERSION)


#	编译Linux环境
linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_linux_release_$(VERSION)



#	编译Windows环境
window:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_windows_release_$(VERSION).exe

osx:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -ldflags="-w -s" -o bin/$(BINARY_NAME)_darwin_release_$(VERSION)