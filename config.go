package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/fs"
	"os"
)

type Config struct {
	SaveDir string `yaml:"SAVE_DIR"`
}

// 默认配置文件
func (c *Config) Default() {
	homeDir, _ := os.UserHomeDir()

	downloadDir := homeDir + string(os.PathSeparator) + "Downloads"

	checkDir(downloadDir)

	c.SaveDir = downloadDir
}

// 读取配置文件
func (c *Config) InitConfig(path string) {
	c.Default()
	file, err := os.ReadFile(path)
	if err != nil {
		homeDir, _ := os.UserHomeDir()
		// 创建默认配置文件
		tmp := "SAVE_DIR: " + c.SaveDir
		err := os.MkdirAll(homeDir+string(os.PathSeparator)+".dm", os.ModePerm)
		if err != nil {
			fmt.Println("默认配置文件夹创建失败")
		}
		f, err := os.OpenFile(homeDir+string(os.PathSeparator)+".dm"+string(os.PathSeparator)+"config.yaml",
			os.O_CREATE|os.O_APPEND|os.O_RDWR,
			os.ModePerm)
		if err != nil {
			fmt.Println("默认配置文件创建失败")
		}
		defer f.Close()

		_, err = f.WriteString(tmp)
		if err != nil {
			fmt.Println("默认配置文件写入失败")
		}

		fmt.Println("配置文件未找到，将使用默认配置文件")
	}
	if err := yaml.Unmarshal(file, c); err != nil {
		fmt.Println("解析配置文件失败，将使用默认配置文件")
	}
	checkDir(c.SaveDir)
}

// 检查文件夹是否存在
func checkDir(downloadDir string) {
	if _, err := os.Stat(downloadDir); os.IsNotExist(err) {
		err = os.MkdirAll(downloadDir, fs.ModePerm)
		if err != nil {
			fmt.Println("下载储存位置不可用，程序退出")
			os.Exit(1)
		}
	}
}
