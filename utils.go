package main

import (
	"crypto/tls"
	"net/http"
)

// HTTP GET 请求
func Get(url string) *http.Response {

	// 跳过证书检查

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	// 实例化 HTTP客户端
	client := &http.Client{
		Transport: tr,
	}
	// 设置请求
	request, _ := http.NewRequest("GET", url, nil)
	// 添加请求头,伪装避免被屏蔽
	// User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.62
	request.Header.Add("User-Agent",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.62")
	// 发送请求
	resp, _ := client.Do(request)
	return resp
}
